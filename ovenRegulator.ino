#include <max6675.h>

// Sample Arduino MAX6675 Arduino Sketch


int ktcSO = 8;
int ktcCS = 9;
int ktcCLK = 10;
double requiredHeat = 27.00;
int led = 13;

MAX6675 ktc(ktcCLK, ktcCS, ktcSO);

  
void setup() {
  Serial.begin(9600);
  // give the MAX a little time to settle
  pinMode(led, OUTPUT);
  delay(500);
}

void loop() {
  // basic readout test
  
   

   double inputHeat = ktc.readCelsius();
   Serial.print("Deg C = "); 
   Serial.print(inputHeat);
   if(inputHeat > requiredHeat){
     digitalWrite(led, HIGH);
   }else{
     digitalWrite(led, LOW);  
   }
    // as a simple test to ensure that the sensor was working I printed out the serial monitor the current temperature.
   
     
 
   delay(500);
}

